using UnityEngine;

public sealed class SpawnEnemy 
{
    private Vector3 _areaSpawn;
    private float _rangeX =5.0f;
    private float _rangeY = 7.4f;

    public SpawnEnemy(Vector3 level)
    {
        _areaSpawn = level;
    }
    public GameObject Spawn(GameObject enemy)
    {
        var newPosition = new Vector3(Random.Range(_areaSpawn.x - _rangeX,_areaSpawn.x+_rangeX),0.0f,Random.Range(_areaSpawn.y - _rangeY,_areaSpawn.y+_rangeY));           
        return Object.Instantiate(enemy, newPosition, Quaternion.identity);
    }
}
