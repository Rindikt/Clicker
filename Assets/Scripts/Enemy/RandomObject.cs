using System.Collections.Generic;
using UnityEngine;

public class RandomObject 
{
   private List<GameObject> list;

    private string path;
    public RandomObject(string path)
    {
        this.path = path;
        list = new List<GameObject>(Resources.LoadAll<GameObject>(path));
    }
    public GameObject GetObject()
    {
        return list[Random.Range(0,list.Count)];
    }
}
