using UnityEngine;
using System;

[RequireComponent(typeof(Rigidbody))]
public sealed class Enemy : MonoBehaviour, IExecute,IFlicker
{
    public event Action<GameObject> OnDestroy = delegate { };
    [SerializeField] float _speed = 30.0f;
    [SerializeField] private int _health;
    [SerializeField] private int _score;
    private Light _light;
    private Rigidbody _rigidbody;
    private int _minHealth = 0;
    private bool _lightOn = true;

    public int Health 
    {
        get 
        {
            return _health; 
        }
    }
    public int Score
    {
        get { return _score; }
    }

    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _light = GetComponent<Light>();
    }
    private void Move()
    {
        _rigidbody.AddForce(UnityEngine.Random.insideUnitCircle*_speed);
    }
    public void GetDemage()
    {
        --_health; 
        Flicker();
        if (_health == _minHealth)
        {
            Destroy();
        }
    }
    // �������� ��� �� �� ������ ���� ��������, �� ������� �� ����� ���������
    public void Destroy()
    {
        OnDestroy.Invoke(gameObject);
        _lightOn = false;
        Destroy(gameObject,0.2f);
    }
    public void Execute()
    {
        Move();
    }
    private void Flicker()
    {
        _light.intensity += 10;
    }
    public void GetSpeed(float speed)
    {
        _speed += speed;
    }
    public void TakeSpeed()
    {
        _speed = 0;
    }


}
