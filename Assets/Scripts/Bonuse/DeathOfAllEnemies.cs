using UnityEngine;

public class DeathOfAllEnemies : InteractiveObject , IExecute , IRotation
{
    private float _speedRotation = 1.0f;
    private Enemy[] _allEnemy;
    private void Update()
    {
        Rotation();
    }

    public void Rotation()
    {
        transform.Rotate(Vector3.up, _speedRotation, Space.World);
    }
    internal override void OnClickObject()
    {
        _allEnemy = FindObjectsOfType<Enemy>();
        foreach (var o in _allEnemy)
        {
            o.Destroy();
        }
        base.OnClickObject();
    }
}
