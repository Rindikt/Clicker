using UnityEngine;

public class FreezeSpeed : InteractiveObject
{
    private Enemy[] _allEnemy;
    internal override void OnClickObject()
    {
        _allEnemy = FindObjectsOfType<Enemy>();
        foreach (var o in _allEnemy)
        {
            o.TakeSpeed();
        }
        base.OnClickObject();
    }
}
