using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExecuteList : IExecute
{
   List<IExecute> executeList;

    public ExecuteList()
    {
        executeList = new List<IExecute>();
    }
    public void Execute()
    {
        foreach (var o in executeList)
        {
            o.Execute();
        }
    }
    public void Add(IExecute execute)
    {
        Debug.Log("Get component " + execute);
        executeList.Add(execute);
    }
}
