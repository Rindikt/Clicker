using UnityEngine;

public class InteractiveObject : MonoBehaviour
{
    internal EnemyController _enemyController;


    private void Update()
    {
        Execute();
    }
    internal virtual void OnClickObject()
    {  
        Destroy(gameObject);
    }
    public virtual void Execute()
    {

    }


}
