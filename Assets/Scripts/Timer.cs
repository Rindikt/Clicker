using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class Timer 
{
    public float _time = 15.0f;
    public Timer()
    {

    }
    public Timer(float time) : base()
    {
        _time = time;
    }

    public bool Start()
    {
        if (_time > 0)
        {
            _time -= 0.1f;
            return false;
        }
        return true;
    }
}
