using UnityEngine;
using UnityEngine.UI;

public class DisplayScore 
{
    private Text _scoreText;
   
    public DisplayScore(GameObject scoreText)
    {
        _scoreText = scoreText.GetComponent<Text>();
        _scoreText.text = "����� �������: ";
    }
    public void Display(int value)
    {
        _scoreText.text = $"����� �������: {value}";
    }
}
