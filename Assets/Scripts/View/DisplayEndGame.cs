
using UnityEngine;
using UnityEngine.SceneManagement;

public sealed class DisplayEndGame 
{
    public GameOverMenuBehaviour _gameOverMenu;
    public DisplayEndGame(GameObject MenuGameOver)
    {

        Debug.Log("конструктор");
        _gameOverMenu = MenuGameOver.GetComponent<GameOverMenuBehaviour>();
        _gameOverMenu._exit.onClick.AddListener(ButtonExitPressed);
        _gameOverMenu._mainMenu.onClick.AddListener(ButtonMainMenuPressed);
    }

    private void ButtonMainMenuPressed()
    {
        Debug.Log("mainMenu");
        SceneManager.LoadScene(0);
    }
    private void ButtonExitPressed()
    {
        Debug.Log("Exit");
        Application.Quit();
    }

}
