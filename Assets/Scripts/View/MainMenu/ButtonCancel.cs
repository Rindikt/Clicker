using UnityEngine;
using UnityEngine.UI;

public sealed class ButtonCancel : MonoBehaviour
{
    public Button button;
    private GameObject _parentObject;


    void Start()
    { 
        button.onClick.AddListener(ButtontPressed);
    }
    private void ButtontPressed()
    {
        Destroy(transform.parent.gameObject);
    }
}
