using UnityEngine;
using UnityEngine.UI;

public sealed class MainMenuBehaviour : MonoBehaviour
{
    public Button _buttonStartGame;
    public Button _buttonRecords;
    public Button _buttonCredits;
    public Button _buttonExit;
}
