using UnityEngine.UI;
using UnityEngine;

public sealed class GameOverMenuBehaviour : MonoBehaviour
{
    public Button _mainMenu;
    public Button _exit;
}
