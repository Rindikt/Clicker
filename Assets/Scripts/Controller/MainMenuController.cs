using System;
using UnityEngine;
using UnityEngine.UI;

public sealed class MainMenuController
{
    public event Action onClickStartGame = delegate { };
    public MainMenuBehaviour _mainMenuButson;
    private bool _visible = true;
    private Reference _reference;



    public MainMenuController(GameObject panelMenu,Reference reference)
    {
        
        _mainMenuButson = panelMenu.GetComponent<MainMenuBehaviour>();
        _mainMenuButson._buttonStartGame.onClick.AddListener(ButtonStartGamePressed);
        _mainMenuButson._buttonRecords.onClick.AddListener(ButtonRecordsPressed);
        _mainMenuButson._buttonCredits.onClick.AddListener(ButtonCreditsPressed);
        _mainMenuButson._buttonExit.onClick.AddListener(ButtonExitPressed);
        _reference = reference;
    }

    //public void ShowMenu()
    //{
    //    if (_visible)
    //    {
    //        _visible = false;

    //    }
    //    else
    //    {
    //        _visible = true;
    //        _mainMenuButson.gameObject.SetActive(true);
    //    }

    //}
 
    private void ButtonStartGamePressed()
    {
        //ShowMenu();
        
        _mainMenuButson.gameObject.SetActive(false);
        onClickStartGame.Invoke();
        
    }
    private void ButtonRecordsPressed()
    {
        Debug.Log(_reference.Record);
    }
    private void ButtonCreditsPressed()
    {
        Debug.Log(_reference.Credits);
    }
    private void ButtonExitPressed()
    {
        Debug.Log("�����");
        Application.Quit();
        
    }

}
