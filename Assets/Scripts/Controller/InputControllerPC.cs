using UnityEngine;

public sealed class InputControllerPC : IExecute
{ 

    RaycastHit hit;
    Ray ray;
    public void Execute()
    {
        test();
    }
    private void test()
    {
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray,out hit,100))
        {
            if (hit.collider.GetComponent<Enemy>()&&Input.GetMouseButtonDown(0))
            {
                hit.collider.GetComponent<Enemy>().GetDemage();
            }
            if ((hit.collider.GetComponent<InteractiveObject>() && Input.GetMouseButtonDown(0)))
            {
                hit.collider.GetComponent<InteractiveObject>().OnClickObject();
            }
        }
    }
}
