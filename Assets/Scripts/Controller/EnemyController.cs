using System;
using System.Collections.Generic;
using UnityEngine;

public sealed class EnemyController : IExecute
{
    public event Action MaxUnitov = delegate { };
    public event Action<GameObject> EnemyIsDead = delegate { };
    private SpawnEnemy _spawnEnemy;
    private Vector3 _position;
    private RandomObject _randomEnemy;
    private List<GameObject> _enemyList;
    private Timer _timer;
    private Enemy _enemy;
    private int _count;
    private const int MAX_UNITOV = 10;
    private const string PATH_TO_ENEMY = "Enemy";
    public float time = 150.0f;
    public float _bonusSpeed = 1.0f;
    private bool _play = true;

    public SpawnEnemy Spawnenemy
    {
        get
        {
            return _spawnEnemy;
        }
    }

    public EnemyController(Vector3 levelPos)
    {
        _position = levelPos;
        Start();
    }
    private void Start()
    {
        _randomEnemy = new RandomObject(PATH_TO_ENEMY);
        _enemyList = new List<GameObject>();
        _spawnEnemy = new SpawnEnemy(_position);
        _timer = new Timer();
    }
    private void EnemyLogic()
    {
        foreach (var i in _enemyList)
        {
            i.GetComponent<Enemy>().Execute();
        }
    }
    private void Timer()
    {
        if (_timer.Start())
        {
            if (Counter() < MAX_UNITOV)
            {
                SpawnEnemy();
                _timer._time = UnityEngine.Random.Range(0.0f, time);
                time -= 0.8f;
                _enemy.GetSpeed(_bonusSpeed);
            }
            else if (Counter() >= MAX_UNITOV && _play)
            {
                _play = false;
                MaxUnitov.Invoke();
                Debug.Log("����� ������ ���?)");
            }
        }
    }

    private void SpawnEnemy()
    {
        var enemy = _spawnEnemy.Spawn(_randomEnemy.GetObject());
        _enemyList.Add(enemy);
        enemy.GetComponent<Enemy>().OnDestroy += Remove;
    }

    private int Counter()
    {
        _count = _enemyList.Count;
        return _count;
    }
    private void Remove(GameObject gameObject)
    {
        EnemyIsDead.Invoke(gameObject);
        _enemyList.Remove(gameObject);
        Debug.Log(_enemyList.Count);
    }
    //public void DeathToTheEnemies()
    //{
    //    foreach (var o in _enemyList)
    //    {
    //      o.GetComponent<Enemy>().Destroy();
    //    }
    //    _enemyList.Clear();
    //}

    public void Execute()
    {
        Timer();
        EnemyLogic();
    }
}
