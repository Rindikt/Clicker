using UnityEngine;

public sealed class GameController : MonoBehaviour
{
    private EnemyController _enemyController;
    private MainMenuController _menuController;
    private InputControllerPC _InputControllerPC;
    private BonuseController _bonusController;
    private DisplayEndGame _displayEndGame;
    private ExecuteList _executeList;
    private DisplayScore _displayScore;
    private Reference _reference;
    private GameObject _level;
    private Vector3 _positionLevel;
    private Score _score;
    private void Awake()
    {
        _executeList = new ExecuteList();
        _InputControllerPC = new InputControllerPC();
        _executeList.Add(_InputControllerPC);
        _reference = new Reference();
        _menuController = new MainMenuController(_reference.MainMenu, _reference);
        _menuController.onClickStartGame += StartGame;

    }
   
    private void Update()
    {
        _executeList.Execute();
    }
    private void StartGame()
    {
        
        _level = Object.Instantiate<GameObject>(_reference.Level);
        _positionLevel = new Vector3(_level.transform.position.x, _level.transform.position.y, _level.transform.position.z);
        _enemyController = new EnemyController(_positionLevel);
        _bonusController = new BonuseController(_enemyController);
        _displayScore = new DisplayScore(_reference.Score);
        _score = new Score(_displayScore);
        _enemyController.MaxUnitov += GameOver;
        _enemyController.EnemyIsDead += _score.GetScore;
        _executeList.Add(_enemyController);
        _executeList.Add(_bonusController);
        Destroy(_menuController._mainMenuButson.gameObject);

    }
    public void GameOver()
    {
        Time.timeScale = 0.0f;
        _displayEndGame = new DisplayEndGame(_reference.MenuGameOver);
        _enemyController.MaxUnitov-=GameOver;
    }

}
