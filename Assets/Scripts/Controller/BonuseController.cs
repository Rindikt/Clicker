using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonuseController : IExecute
{
    EnemyController _enemyController;
    private SpawnEnemy _spawnBonuse;
    private RandomObject _randomBonuse;
    private Timer _timer; 
    private List<GameObject> _list;
    private const string PATH_TO_BONUSE = "Bonuse";
    private float startTime = 90.0f;
    public BonuseController(EnemyController enemyController)
    {
        _enemyController = enemyController;
        _randomBonuse = new RandomObject(PATH_TO_BONUSE);
        _spawnBonuse = _enemyController.Spawnenemy;
        _timer = new Timer(startTime);
        _list = new List<GameObject>();

    }
    private void SpawnBonuse()
    {
        var bonuse =_spawnBonuse.Spawn(_randomBonuse.GetObject());
       
        _list.Add(bonuse);

    }

    public void Execute()
    {
        Timer();
        //foreach (var i in _list)
        //{
        //    i.GetComponent<InteractiveObject>().Execute();
        //}
    }
    private void Timer()
    {
        if (_timer.Start())
        {
            SpawnBonuse();
            _timer._time = Random.Range(500,1000);
        }
    }
}
