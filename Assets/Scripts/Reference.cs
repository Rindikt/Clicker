using UnityEngine;

public sealed class Reference
{
    private Canvas _canvas;
    private GameObject _level;
    private GameObject _mainMenu;
    private GameObject _records;
    private GameObject _credits;
    private GameObject _enemy;
    private GameObject _score;
    private GameObject _menuGameOver;

    public GameObject MenuGameOver
    {
        get {
            if (_menuGameOver == null)
            {
                var gameOver = Resources.Load<GameObject>("UI/GameOverMenu");
                Object.Instantiate(gameOver,Canvas.transform);
            }
            return _menuGameOver; }
    }

    public GameObject Score
    {
        get
        {
            if (_score == null)
            {
                var sc = Resources.Load<GameObject>("UI/Score");
                _score = Object.Instantiate(sc,Canvas.transform);
            }
            return _score;
        }
    }

    public GameObject Credits
    {
        get
        {
            if (_credits == null)
            {
                var credits = Resources.Load<GameObject>("UI/Credits");
                _credits = Object.Instantiate(credits,Canvas.transform);
            }
            return _credits;
        }
    }
    public GameObject Record
    {
        get
        {
            if (_records == null)
            {
                var records = Resources.Load<GameObject>("UI/Records");
                _records = Object.Instantiate(records,Canvas.transform);
                
            }
            return _records;
        }
    }
    public GameObject MainMenu
    {
        get 
        {
            if (_mainMenu == null)
            {
                var menu = Resources.Load<GameObject>("UI/MainMenu");
                _mainMenu = Object.Instantiate(menu, Canvas.transform);
            }
            return _mainMenu; }
    }

    public GameObject Level
    {
        get 
        {
            if (_level == null)
            {
                _level = Resources.Load<GameObject>("Prefab/Level");
            }
            return _level; }
    }
    
    public Canvas Canvas
    {
        get 
        {
            if (_canvas == null)
            {
                var canvas = Resources.Load<Canvas>("UI/Canvas");
                _canvas = Object.Instantiate(canvas);
            }
            return _canvas; 
        }
    }
}
