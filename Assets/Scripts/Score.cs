using UnityEngine;

public sealed class Score
{
    private int _score;
    DisplayScore _displayScore;
    public Score(DisplayScore displayScore)
    {
        _displayScore = displayScore;
    }
    public int CountScore
    {
        get { return _score; }
    }
    public void GetScore(GameObject enemy)
    {
        _score += enemy.GetComponent<Enemy>().Score;
        _displayScore.Display(_score);
    }
}
