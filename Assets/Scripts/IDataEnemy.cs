using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Data",fileName ="DataEnemy")]
public sealed class IDataEnemy : ScriptableObject
{
    [SerializeField] private GameObject _enemyPrefab;

    private float _health;
    private float _Speed;
}
